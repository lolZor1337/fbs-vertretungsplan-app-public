var pageState = {
	LOGINPAGE: {},
	NOLOGINPAGE: {},
	MAINPAGE: {},
	DETAILPAGE: {},
	INFOPAGE: {}
};
var currentPage;
var mainPage = 'main.html';
var loginPage = 'index.html';

$(function() {
	//deviceready wird aufgerufen, wenn das Gerät die Seite geladen hat
	document.addEventListener("deviceready", onDeviceReady, false);
});

/**
 * Events die eingebunden werden, sobald das Gerät fertig geladen hat 
 */
function onDeviceReady() {
    document.addEventListener("resume", onResume, false);
    document.addEventListener("backbutton", onBackButton, false);
}

/**
 * Wenn die App aus dem Hintergrund geöffnet wird, soll die Seite einmal neu geladen werden, falls man auf Unterseiten war, gelangt man auf die Hauptseite
 */
function onResume() {
	if(currentPage == pageState.MAINPAGE) {
		refreshPage();	
	} else if(currentPage == pageState.DETAILPAGE) {
		window.location.href = mainPage;
	} else if(currentPage == pageState.NOLOGINPAGE) {
    	window.location.href = loginPage;
    }
}

/**
 * Auf verschiedenen App-Seiten werden unterschiedliche Aktionen ausgeführt, beim Klick auf den Zurück-Button 
 */
function onBackButton() {
    if(currentPage == pageState.MAINPAGE) {
    	logout();
    } else if(currentPage == pageState.LOGINPAGE) {
    	if (navigator.app) {
		    navigator.app.exitApp();
		} else if (navigator.device) {
		    navigator.device.exitApp();
		} else {
		    window.close();
		}
    } else if(currentPage == pageState.DETAILPAGE || currentPage == pageState.INFOPAGE) {
    	window.location.href = mainPage;
    } else if(currentPage == pageState.NOLOGINPAGE) {
    	window.location.href = loginPage;
    }
    //TODO schauen, ob es von fr7 eine Funktion gibt einen sauberen Übergang zu bekommen
}

/**
 * Sendet eine Anfrage mit Benutzername und Passwort als Parameter 
 */
function loginUser() {
	var username = $('#username').val();
	var password = $('#password').val();
	
	myApp.showIndicator();
	
	$.ajax({
		type: 'POST',
		url: 'https://fbs-moodle.schulen-fulda.de/moodle/login/token.php',
		data: {username: username, password: password, service: 'moodle_mobile_app'},
		success: loginCallback,
		error: loginError,
		timeout: 20000	//timeout auf 20 Sekunden, danach kommt eine Fehlermeldung
	});
	
	saveUserData(username, password);
	saveFirstName(username);
}


function saveFirstName(username) {
	/* Setze den Vorname für direkte Useranrede */

    var fullName = username;
    var dotPos = fullName.indexOf(".");
    var plainFirstName = fullName.substring(0,dotPos);
    var firstChar = plainFirstName.substring(0,1).toUpperCase();
    var firstName = firstChar + plainFirstName.substring(1,plainFirstName.length).toLowerCase();
    localStorage.setItem('firstName',firstName);
}


/**
 * Speichert Benutzername und Passwort lokal, falls die Checkbox gehakt wurde
 */
function saveUserData(username, password) {
	if($('#save-data').is(':checked')) {
		localStorage['username'] = username;
		localStorage['password'] = password;
		localStorage['save-data'] = 'true';
	} else {
		localStorage.clear();
		localStorage['save-data'] = 'false';
	}
}

/**
 * Lädt Passwort und Benutzername falls diese Daten existieren.
 * Zusätzlich ändert es den Zustand der Checkbox, falls zuvor der Haken entfernt wurde.
 */
function parseUserData() {
	if(localStorage['save-data'] == 'false') {
		$('#save-data').prop('checked', false);
	}
	if(localStorage['username'] != undefined && localStorage['password'] != undefined) {
		$('#username').val(localStorage['username']);
		$('#password').val(localStorage['password']);
		
		//Password-Show-Icon einblenden, wenn Passwort nicht leer ist
		if($('#password').val() != '') {
			$('#password_show_icon').show();
		}
	}
}

/**
 * Funktionen blendet Password-Show-Icon ein oder aus 
 */
function passwordIconEvent() {
	if($(this).val() != '') {
		$('#password_show_icon').show();
	} else {
		$('#password_show_icon').hide();
	}
}

/**
 * Gibt eine Statusänderung an, dass keine Internetverbindung existiert 
 */
function loginError() {
	myApp.hideIndicator();
	changeLoginStatus('Das Gerät benötigt eine funktionierende Internetverbindung.');
}

/**
 * Login-Callback prüft, ob 'token' existiert, wenn Ja, wird auf main.html weitergeleitet 
 */
function loginCallback(data) {
	myApp.hideIndicator();
	if(data.errorcode == 'invalidlogin' || data.errorcode == 'auth_ldap_noconnect_all') {
		changeLoginStatus('Das eingegebene Passwort stimmt nicht mit dem Benutzernamen überein.');
	} else if(data.token != undefined) {
		localStorage['token'] = data.token;
		window.location = mainPage;	
	} else {
		changeLoginStatus('Login zurzeit nicht möglich. Du kannst es in einigen Minuten erneut versuchen.');
	}
}

/**
 * Diese Funktion schickt eine Anfrage an die token.php der Schule,
 * kommt eine Antwort zurück hat das Gerät eine Internetverbindung, anstonsten gibt sie false zurück 
 */
function hasEthernetConnection() {
	return true;//TODO prüfen, ob Internetverbindung existiert
}

/**
 * Gibt true zurück, falls der Benutzer eingeloggt ist. 
 */
function isLoggedIn() {
	return hasEthernetConnection() && localStorage['token'] != undefined;
}

/**
 * Entfernt den 'token' und leitet auf die Login-Seite weiter 
 */
function logout() {
	myApp.showIndicator();
	localStorage.removeItem('token');
	window.location.href = loginPage;		
}

/**
 * Ändert den Status, um dem Benutzer eventuelle Fehler ordentlich darzustellen 
 */
function changeLoginStatus(text) {
	$('#login-status').html(text).fadeOut(200, function() {
		$(this).fadeIn(200);
	});
}