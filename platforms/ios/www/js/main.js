var contentArrayForData = [];
var errorShown = false;
var accessToken = '';
//TOKEN AUFGRUND VON SICHERHEITSMASNAHMEN ENTFERNT

$(function() {

	/* verfügbare Vertretungspläne anfragen */
	requestAvailablePlans();
    
    /* Pläne über Datum auf- und zuklappbar machen */
	$('#plandate_1, #plandate_2').click(collapsePlan);
    
    /* Seite aktualisieren */
    $('#refresh-button').click(refreshPage);
    
    /* Setzt die Willkommensnachricht */
   	var firstName = localStorage.getItem('firstName');
   	if(firstName != null && firstName != undefined) {
		$('#welcomeMessage').html('Hallo ' + localStorage.getItem('firstName') + ',<br>folgende Vertretungspläne stehen für dich bereit:');
   	}
});

/**
 * Bei Ajax-Error wird hier eine Fehlermeldung ausgegeben, falls kein Plan aktiv ist
 */
function errorCallback() {
	activePlans = 0;
	$('#planhtml_1, #planhtml_2').each(function() {
		if($(this).attr('rel') == 'filled') {
			activePlans++;
		}
	});
	
	if(activePlans == 0 && !errorShown) {
		errorShown = true;
		$('#welcomeMessage').fadeOut();
		myApp.alert('Es gab Fehler beim Abrufen der aktuellen Vertretungspläne.');
		$('.page-content, #error_notice').fadeIn();
	}
}

/**
 * Funktion um Seite neu zu laden
 */
function refreshPage() {
	$(this).fadeOut(200);
	myApp.showIndicator();
	setTimeout(function() {
		if(isLoggedIn()) {
			location.reload();
		} else {
			logout();
		}		
	}, 500);
}

/**
 * Wandelt das JSON-Objekt in HTML um und fügt es an eine Tabelle an
 */
function parseJSON(json, planID) {
	var classNamePosition = [];
	var classContent = [];
	
	var lastGrade = '';
	var output = '';
	output += '<ul>';
	$.each(json, function(grade) {
		for(var i = 1; i < 6; i++) {
			classContent[grade] +=  '<th>' + json[grade][i] + '</th>';
			classNamePosition[grade] = json[grade][0];
		}
		
		if(json[grade][0] != '&nbsp;') {
			output += '<li><a href="showTabelle.html" onclick=\'load("'+json[grade][0]+'", '+planID+')\' class="item-link item-content"><div class="item-inner"><div class="item-title" >' + json[grade][0] + '</div></div></a></li>';   
		}
	});
	output += '</ul>';
	$('#planhtml_'+planID).append(output).attr('rel', 'filled').parent().parent('.delimiter-container').show();
	$('.page-content').fadeIn();
	contentArrayForData[planID+'_positions'] = classNamePosition;
	contentArrayForData[planID+'_content'] = classContent;
}

/**
 * Läd die Daten für die Klasse die im Detail angezeigt werden soll 
 */
function load(gradename, planID) {
	var classNamePosition = contentArrayForData[planID+'_positions'];
	var classContent = contentArrayForData[planID+'_content'];
	
	currentPage = pageState.DETAILPAGE;
	$("#classTabelle").html("");
	var classContentinnerHTML = '';
	classContentinnerHTML += '<tr><th>Std.</th><th>Lehrer</th><th>Raum</th><th>V.-Lehrer</th><th>Art</th></tr>';
	var detectGrade = false;
	for(var i = 0; i < classNamePosition.length; i++){
		if(classNamePosition[i] == gradename){
			classContentinnerHTML += "<tr>";
			classContentinnerHTML += classContent[i];
			classContentinnerHTML += "</tr>";
			detectGrade = true;
		}
		if(classNamePosition[i] == '&nbsp;' && detectGrade == true){
			classContentinnerHTML += "<tr>";
			classContentinnerHTML += classContent[i];
			classContentinnerHTML += "</tr>";
		}
		if(classNamePosition[i] != '&nbsp;' && classNamePosition[i] != gradename && detectGrade == true){
			break;
		}
	}
	
	setTimeout(function () {
		$('.center.klassenHeader').html("Klasse: " + gradename);
		$('#classTabelle').html(classContentinnerHTML);
	}, 500);
}

/**
 * Gibt das Datum des aktuellen Vertretungsplans zurück
 */
function getDate(html) {
	var regex = /(?:<h1 class="list-table-caption">(.*?)<\/h1>)/g;
	return regex.exec(html)[1];
}

/**
 * Wandelt den übergebenen String in ein valides JSON-Format um
 */
function buildJSON(html) {
	var json = '{';
    var rowCounter = 0;

    var rowRegex = /(?:<tr>(.*?)<\/tr>)/g;
	while(row = rowRegex.exec(html)) {
		row = row[1];
		var dataRegex = /(?:<td>(.*?)<\/td>)/g;
    	var dataCounter = 0;
    	json += '"' + rowCounter + '": {';
		while(data = dataRegex.exec(row)) {
			data = data[1];
			json += '"' + dataCounter + '": "' + data + '",';
			dataCounter++;
		}
		if(json.lastIndexOf(',')+1 == json.length) {
			json = json.slice(0, -1);
		}
		json += '},';
		rowCounter++;
	}
	json = json.slice(0, -1);
	json += '}';
	return $.parseJSON(json);
}

/**
 * Gibt den übergebenen String ohne Leerzeichen und Zeilenumbrüche zurück
 */
function replaceLineBreaks(html) {
    return html.replace(/(\r\n|\n|\r)/g, '');
}

/**
 * Läd - wenn möglich - zwei Vertretungspläne herunter und läd sie auf die Seite  
 */
function requestPlan(planID) {
	var timestamp = Date.now();
    $.ajax({
    	url: 'https://fbs-moodle.schulen-fulda.de/moodle/get_plan.php?access_token='+accessToken+'&plan_id='+planID+'&t='+timestamp,
        // url: 'https://fbs-moodle.schulen-fulda.de/Vertretung/V_DC_00'+planID+'.html?t='+timestamp,
        success: function(data) {
            data = replaceLineBreaks(data);
            
			var regex = /(?:<tbody>(.*?)<\/tbody>)/g;
			var tableContent = regex.exec(data)[0];
            var json = buildJSON(tableContent);
            parseJSON(json, planID);
            
            $('#plandate_'+planID).html(getDate(data));
        },
        error: errorCallback,
		timeout: 20000	//timeout auf 20 Sekunden, danach kommt eine Fehlermeldung
    });	
}

/**
 * Wird beim Klick auf ein Datum aufgerufen und klappt den Plan ein- oder aus 
 */
var timeout;
function collapsePlan() {
    if(timeout) {
        return false;
    }
    timeout = true;
	$(this).toggleClass('open');
	
	var rel = $(this).attr('rel');
	$('#'+rel).slideToggle();
	$(this).siblings('.arrowDown').toggleClass('active');
    setTimeout(function() {
        timeout = false;
    }, 400);
}

/**
 * Anhand der Übersichtsseite prüfen, welche Pläne geladen werden können 
 */
function requestAvailablePlans() {
	var timestamp = Date.now();
    $.ajax({
	    url: 'https://fbs-moodle.schulen-fulda.de/moodle/get_plan.php?access_token='+accessToken+'&plan_id=overview&t='+timestamp,
        // url: 'https://fbs-moodle.schulen-fulda.de/Vertretung/index.html',
        success: function(data) {
            data = replaceLineBreaks(data);
            
            var matches = data.match(/<input .*? onClick="window.location.href='(V_DC_00\d.html)'">/g);
            
            if(matches != null && matches.length > 0) {
	            for(var i = 1; i <= matches.length; i++) {
	            	requestPlan(i);
	            }
            } else {
            	$('#welcomeMessage').hide();
            	$('#error_notice').html('Zurzeit stehen keine Vertretungspläne zur Verfügung.').fadeIn();
            	$('.page-content').fadeIn();
            }
        }
    });
}