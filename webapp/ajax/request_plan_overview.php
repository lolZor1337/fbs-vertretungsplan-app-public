<?php
include_once("../classes/class_login.php");
start();

$access_token = "";
//TOKEN AUFGRUND VON SICHERHEITSMASNAHMEN ENTFERNT

if(!is_logged_in()) {
    exit("recoverablesession_error");
}

$timestamp = time();

$url = "fbs-moodle.schulen-fulda.de/moodle/get_plan.php?access_token=".$access_token."&plan_id=overview&t=".$timestamp;
// $url="https://fbs-moodle.schulen-fulda.de/Vertretung/index.html";
$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, $url);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

$content = curl_exec($curl);

if(empty($content)) {
    exit("recoverableerrorcode");
}
exit($content);
?>