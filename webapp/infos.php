<?php
include ("classes/class_login.php");
start();

if (!is_logged_in()) {
	redirect_to_login();
}
?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Erforderlich meta Tags-->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, minimal-ui">
		<meta name="mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<meta name="theme-color" content="#2196f3">
		<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
		<title>FBS-Vertretungsplan | Informationen</title>
		<link rel="stylesheet" href="css/bootstrap.min.paper.css" >
		<link rel="stylesheet" href="css/webappStyle.css" >
		<link rel="icon" href="img/icon.png">
	</head>
	<body>

		<nav class="navbar navbar-inverse navbar-fixed-top " role="navigation">
			<div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<a class="navbar-brand" style="color:white;" href="#"><b>Vertretungsplan</b></a>
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li>
							<a href="#" onclick="window.location.href='index.php'" style="color: white;font-size: 17px;" id="homeNavbar" >Home</a>
						</li>
						<li>
							<a class="active" href="#" onclick="window.location.href='infos.php'" style="color: white;font-size: 17px;" href="infos.php" id="infosNavbar" >Infos</a>
						</li>
						<li>
							<a href="#" id="logout_button" style="color: white;font-size: 17px;" >Abmelden</a>
						</li>
					</ul>
				</div>
			</div>
		</nav>
		<br>
		<br>
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-md-offset-7">
					<div class="panel panel-default">
						<div class="panel-heading">
							Informationen
						</div>
						<div class="panel-body">
							<div >

								<h5>Allgemeine Hinweise</h5>
								<p>
									Diese App wurde im Rahmen eines schulischen Projektes für die Ferdinand-Braun-Schule Fulda entwickelt.
								</p>

								<h5>Weiterverwendung</h5>
								<p>
									Der Source-Code der App ist öffentlich verfügbar und kann gerne weiterverwendet werden.
								</p>

								<h5>Anregungen</h5>
								<p>
									Anregungen und Probleme können an folgende E-Mail Adresse geschickt werden: <a class="external" href="mailto:fbs.fulda.dev@gmail.com">fbs.fulda.dev@gmail.com</a>
								</p>

								<h5>Entwickler der App:</h5>
								<ul>
									<li>
										Alexander Propp
									</li>
									<li>
										Lorenz Hohmann
									</li>
									<li>
										Tim Blochwitz
									</li>
								</ul>
        						<p><strong>Betreuende Lehrkraft:</strong> Jörg Reuter</p>
							</div>
						</div>
					</div>
					<a href="#" class="bck-btn btn btn-primary btn-lg" onclick="window.location.href='index.php'" >Zurück</a>
				</div>
			</div>

			<!-- Pfade zu allen Erforderlichen JS-Dateien -->
			<script type="text/javascript" src="vendor/jQuery/jquery.min.js"></script>
			<script type="text/javascript" src="vendor/fr7/js/framework7.js"></script>
			<script type="text/javascript" src="vendor/fr7/js/fw7_settings_webapp.js"></script>
			<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
			<script type="text/javascript" src="js/main.js"></script>
	</body>
</html>
