<html>
	<head>
		<!-- Erforderlich meta Tags-->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, minimal-ui">
		<meta name="mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<meta name="theme-color" content="#2196f3">
		<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
		<title>FBS-Vertretungsplan | Probleme bei der Anmeldung</title>
		<link rel="stylesheet" href="css/webappStyle.css">
		<link rel="stylesheet" href="css/bootstrap.min.paper.css" >
		<link rel="icon" href="img/icon.png">
	</head>
	<body >

		<nav class="navbar navbar-inverse navbar-fixed-top " role="navigation">
			<div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<a class="navbar-brand" style="color:white;" href="#"><b>Vertretungsplan</b></a>
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav"></ul>
				</div>
			</div>
		</nav>

		<br>
		<br>
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-md-offset-7">
					<div class="panel panel-default">
						<div class="panel-heading">
							Probleme bei der Anmeldung?
						</div>
						<div class="panel-body">
							<div >
								<h5>Ich habe keinen Moodle-Zugang</h5>
								<p>
									Generell hat jeder Schüler einen persönlichen Moodle-Zugang. Dein Klassenlehrer kann dir erklären, welche Login-Daten du für den ersten Login benutzen kannst.
								</p>
								<br>
								<h5>Ich habe mein Passwort vergessen</h5>
								<p>
									Du kannst dein Passwort <a class="external" target="_blank" href="https://fbs-moodle.schulen-fulda.de/moodle/login/forgot_password.php">auf dieser Seite</a> zurücksetzen
								</p>
								<br>
								<h5>Ich habe eine andere Frage</h5>
								<p>
									Hierzu kannst du eine E-Mail an <a class="external" href="mailto:joerg@reuter.sc">Herr Jörg Reuter</a> verfassen und ihm dein Problem genau schildern.
								</p>
							</div>
						</div>
					</div>
					<a class="bck-btn btn btn-primary btn-lg" onclick="window.location.href='login.php'" >Zurück</a>
				</div>
			</div>

			<!-- Pfade zu allen Erforderlichen JS-Dateien -->
			<script type="text/javascript" src="vendor/jQuery/jquery.min.js"></script>
			<script type="text/javascript" src="vendor/fr7/js/framework7.js"></script>
			<script type="text/javascript" src="vendor/fr7/js/fw7_settings_webapp.js"></script>
			<script src="js/main.js" type="text/javascript"></script>
	</body>
</html>