<?php
function start() {
	session_name("fbs-webapp");
	session_start();
}

function redirect_to_login($ajax=false) {
	header("Location: ".($ajax?"../":"")."login.php");
	exit();
}

function redirect_to_main($ajax=false) {
	header("Location: ".($ajax?"../":"")."index.php");
	exit();
}

function is_logged_in() {
	return isset($_SESSION["token"]) && !empty($_SESSION["token"]);
}

function login($username, $password) {

	$url="fbs-moodle.schulen-fulda.de/moodle/login/token.php?username=".$username."&password=".$password."&service=moodle_mobile_app";
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

	$authentication = curl_exec($curl);
	if($authentication) {
		$json = json_decode($authentication); 
		if(isset($json->token) && !empty($json->token)) {
			$_SESSION["token"] = $json->token;
			return true;
		}
	}
	return false;
}

function logout() {
	unset($_SESSION["token"]);
}

?>