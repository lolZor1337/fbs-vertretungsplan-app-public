<?php
include_once ("classes/class_login.php");
start();

if (is_logged_in()) {
	redirect_to_main();
}
?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Erforderlich meta Tags-->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, minimal-ui">
		<meta name="mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<meta name="theme-color" content="#2196f3">
		<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
		<title>FBS-Vertretungsplan | Anmeldung</title>
		<link rel="stylesheet" href="css/webappStyle.css">
		<link rel="stylesheet" href="css/bootstrap.min.paper.css" >
		<link rel="icon" href="img/icon.png">
	</head>
	<body >

		<nav class="navbar navbar-inverse navbar-fixed-top " role="navigation">
			<div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<a class="navbar-brand" style="color:white;" href="#"><b>Vertretungsplan</b></a>
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1"></div><!-- /.navbar-collapse -->
			</div><!-- /.container-fluid -->
		</nav>
		<br>
		<br>
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-md-offset-7">
					<div class="panel panel-default">
						<div class="panel-heading">
							Anmeldung
						</div>
						<div class="panel-body">
							<div style="margin-bottom: 0;" class="login-screen-title"><img src="img/FBS_Logo_transparent.png" style="display:block; margin-left:auto; margin-right:auto;" alt="FBS Logo" width="120px">
							</div>
							<div id="login-status"></div>
							<form method="post" class="form-horizontal" role="form">
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-3 control-label"> Benutzername</label>
									<div class="col-sm-9">
										<input class="form-control" type="text" name="username" id="username" placeholder="Benutzername" required>
									</div>
								</div>
								<div class="form-group">
									<label for="inputPassword3" class="col-sm-3 control-label"> Passwort</label>
									<div class="col-sm-9">
										<input class="form-control" type="password" name="password" id="password" placeholder="Passwort" required>
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-offset-3 col-sm-9">
										<div class="checkbox">
											<label>
												<input type="checkbox" checked="checked" id="save-data" name="save-data"/>
												<label for="save-data">Anmeldedaten speichern</label> </label>
										</div>
									</div>
								</div>
								<div class="form-group last">
									<div class="col-sm-offset-3 col-sm-9">
										<a class="btn btn-success btn-sm"><span href="#" id="login-button" style:"color:white;" value="anmelden" >Anmelden</span></a>
									</div>
								</div>
							</form>
						</div>
						<div class="panel-footer">
							<div>
								Nutze deinen Moodle-Zugang um dich hier anzumelden.
							</div>
							<a href="#" onclick="window.location.href='no_login.php'" class="problem_button">Probleme bei der Anmeldung?</a>
						</div>
					</div>
				</div>
			</div>
			<!-- Pfade zu allen Erforderlichen JS-Dateien -->
			<script type="text/javascript" src="vendor/jQuery/jquery.min.js"></script>
			<script type="text/javascript" src="vendor/fr7/js/framework7.js"></script>
			<script type="text/javascript" src="vendor/fr7/js/fw7_settings_webapp.js"></script>
			<script src="js/main.js" type="text/javascript"></script>
			<script>
				$(function() {
					parseUserData();
				});
			</script>
	</body>
</html>