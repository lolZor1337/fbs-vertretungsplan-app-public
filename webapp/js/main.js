var contentArrayForData = [];
var errorShown = false;

$(function() {
    /* Seite aktualisieren */
    $('#refresh-button').click(refreshPage);

	/* Login-Button triggern */
	$('#login-button').click(loginUser);

	/* keyup-Event auf Passworteingabe binden um Anzeige-Icon ein-/auszublenden */
	$('#password').keyup(passwordIconEvent);

	/* Passwort-Input auf text und password ändern */
	$('#password_show_icon').click(function() {
		$(this).toggleClass('active');
		if($(this).hasClass('active')) {
			$('#password').attr('type', 'text');
		} else {
			$('#password').attr('type', 'password');
		}
	}); 

	/* Benutzer einloggen wenn er im Passwort-Feld Enter drückt */
	$('#password').keypress(function(e) {
		if(e.which == 13) {
			loginUser();
		}
	});

	/* Logout-Button triggern */
	$('#logout_button').click(logoutUser);
});

/**
 * Funktion um Seite neu zu laden
 */
function refreshPage() {
	$(this).fadeOut(200);
	myApp.showIndicator();
	setTimeout(function() {
		location.reload();		
	}, 500);
}

/**
 * Funktionen blendet Password-Show-Icon ein oder aus 
 */
function passwordIconEvent() {
	if($(this).val() != '') {
		$('#password_show_icon').show();
	} else {
		$('#password_show_icon').hide();
	}
}

/**
 * Speichert Benutzername und Passwort lokal, falls die Checkbox gehakt wurde
 */
function saveUserData(username, password) {
	if($('#save-data').is(':checked')) {
		localStorage['username'] = username;
		localStorage['password'] = password;
		localStorage['save-data'] = 'true';
	} else {
		localStorage.clear();
		localStorage['save-data'] = 'false';
	}
}

/**
 * Lädt Passwort und Benutzername falls diese Daten existieren.
 * Zusätzlich ändert es den Zustand der Checkbox, falls zuvor der Haken entfernt wurde.
 */
function parseUserData() {
	if(localStorage['save-data'] == 'false') {
		$('#save-data').prop('checked', false);
	}
	if(localStorage['username'] != 'undefined' && localStorage['password'] != 'undefined') {
		$('#username').val(localStorage['username']);
		$('#password').val(localStorage['password']);
		
		//Password-Show-Icon einblenden, wenn Passwort nicht leer ist
		if($('#password').val() != '') {
			$('#password_show_icon').show();
		}
	}
}

/**
 * Ändert den Status, um dem Benutzer eventuelle Fehler ordentlich darzustellen 
 */
function changeLoginStatus(text, color) {
	$('#login-status').html(text).fadeOut(200, function() {
		$(this).fadeIn(200);
		
		$(this).removeClass('red').removeClass('green');
		if(color == 'green') {
			$(this).addClass('green');
		} else if(color == 'red') {
			$(this).addClass('red');
		}
	});
}

/**
 * Schickt Ajax-Anfrage um Benutzer einzuloggen
 */
function loginUser() {
	changeLoginStatus('Prüfe Login...', 'green');
	var username = $('#username').val();
	var password = $('#password').val();
	$.post('ajax/request_login.php', {username: username, password: password}, function(data) {
		if(data == 'success') {
			changeLoginStatus('Leite auf Übersicht weiter...', 'green');
			saveUserData(username, password);
			window.location.href = 'index.php';
		} else if(data == 'incorrect_data') {
			changeLoginStatus('Das eingegebene Passwort stimmt nicht mit dem Benutzernamen überein.', 'red');
		} else {
			changeLoginStatus('Login zurzeit nicht möglich. Du kannst es in einigen Minuten erneut versuchen.', 'red');
		}
	});
}

/**
 * Schickt Ajax-Anfrage um Benutzer auszuloggen
 */
function logoutUser() {
	myApp.showIndicator();
	$.post('ajax/request_logout.php', function(data) {
		myApp.hideIndicator();
		if(data == 'success') {
			window.location.href = 'login.php';
		}
	});
}